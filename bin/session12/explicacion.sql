USE libreria;

-- PROYECCIÓN
select vtaFecha, vtaCantidad, vtaTotal from venta;

-- SELECCIÓN NUMEROS, FECHAS O COMPARACIONES NÚMERICAS
select * from venta where vtaFecha>'2020-05-01';

-- SELECCIÓN CARACTERES
select * from autor where autPais = 'Colombia';
select * from autor where autApellido like '%García%';
 -- SELECT * FROM genba_hseqasesorias.historial where ACTOR LIKE '%victor.cardenas%' AND MOMENTO LIKE '%2021-10-08%';
 
-- COMPOSICION
select autNombre, autApellido from autor where autPais = 'Colombia' order by autApellido desc limit 3;
 
 -- CONSULTAS AGREGACIONES
 select vtaTotal from venta;
 select libNombre, libPrecio from libro where libPrecio = (select max(libPrecio) from libro);
 
 SELECT libId, count(vtaId) FROM venta group by libId;
 
 -- RENOMBRAR
 SELECT libId, count(vtaId) as 'Cantidades de venta' FROM venta group by libId;
 
 -- PRODUCTO CARTESIANO
 select * from libro;
 select * from editorial;
 SELECT * FROM libro,editorial;
 -- PRODUCTO CARTESIANO CON COMBINACIONES CORRECTAS
 SELECT * FROM libro,editorial, autor WHERE libro.ediId = editorial.ediId AND autor.autId = libro.autId;
 SELECT libId, libNombre, libPub as 'Año de publicación', ediNombre, autNombre, autApellido FROM libro,editorial, autor WHERE libro.ediId = editorial.ediId AND autor.autId = libro.autId;
 
 -- JOIN
 SELECT libId, libNombre, libPub as 'Año de publicación', ediNombre, autNombre, autApellido FROM libro JOIN editorial using (ediId) join autor using (autId);
 
 -- NATURAL JOIN 
 SELECT * FROM libro natural JOIN editorial NATURAL join autor natural join venta;
 
 -- OTRAS CONSULTAS
SELECT * from autor;
SELECT DISTINCT autPais AS "Paises" FROM autor ;
select * from venta order by vtaFecha;
SELECT SUM(vtaCantidad) as "Cantidad de libros vendidos desde el 1 de enero de 2021" from venta where year(vtaFecha)>2020;

SELECT concat(autNombre, concat(" ",autApellido))as "Nombre completo del autor" , count(libId) as "Cantidad de libros"  FROM libro natural join autor group by autId;

SELECT libNombre, sum(vtaCantidad) as 'Total' from libro natural join venta group by libId order by Total desc;