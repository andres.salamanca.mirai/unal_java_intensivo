package session04;

public class EjercicioFunciones {

	public static void main(String[] args) {
		double a1 = 5, b1 = 10, a2 = 4, b2 = 8, r1 = 5, r2 = 3;

		double primerRectangulo = areaRectangulo(b1, a1);
		double segundoRectangulo = areaRectangulo(b2, a2);

		double primeraRueda = areaCirculo(r1);
		double segundaRueda = areaCirculo(r2);

		double areaTotal = primerRectangulo + segundoRectangulo + primeraRueda + segundaRueda;
		
		System.out.println(areaTotal);
	}

	public static double areaRectangulo(double base, double altura) {
		return base * altura;
	}

	public static double areaCirculo(double radio) {
//		Math.pow(radio, 2);
		return Math.PI * radio * radio;

	}

}
