package session04;

import javax.swing.JOptionPane;

public class Ejercicio2Funciones {
	
	/**
	 * Si pido prestados P cantidad de pesos para pagarlos en dos meses, si el 
	 * interes del prestamo es del 3%. ¿Cuanto se debe pagar al final del segundo
	 *  mes si el interes es compuesto mensualmente?.
	 */
	
	private static double prestamoInicial;
	private static double valorActual = 0;
	private static int meses = 6;
	private static double interes = 0.02;

	public static void main(String[] args) {
		prestamoInicial = Double.parseDouble(JOptionPane.showInputDialog("Valor del prestamo"));
		meses = Integer.parseInt(JOptionPane.showInputDialog("Cantidad de meses"));
		interes = Double.parseDouble(JOptionPane.showInputDialog("Ingrese el porcentaje de la tasa de interes"))/100;
		
		valorActual = prestamoInicial;
		for (int i =0 ; i<= meses;i++) {
			valorActual = calculoInteres(i);
			System.out.println(valorActual);
		}
		
		JOptionPane.showMessageDialog(null, "El valor total a pagar del prestamo es: " + valorActual);
	}
	
	public static double calculoInteres(int mesActual) {
		return prestamoInicial * Math.pow((1+interes), mesActual); 
	}

}
