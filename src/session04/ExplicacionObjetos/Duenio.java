package session04.ExplicacionObjetos;

public class Duenio {

	private String nombre;
	private String celular;
	private String correo;

	public Duenio(String nombre, String celular, String correo) {
		this.nombre = nombre;
		this.celular = celular;
		this.correo = correo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	

}
