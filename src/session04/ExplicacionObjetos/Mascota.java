package session04.ExplicacionObjetos;

import javax.swing.JOptionPane;

public class Mascota {

	private String tipo;
	private String nombre;
	private String raza;
	private Duenio duenio;
	
	public Mascota(String tipo, String nombre, String raza) {
		this.tipo = tipo;
		this.nombre = nombre;
		this.raza = raza;
		String nombreDelDuenio = JOptionPane.showInputDialog("Nombre del dueño");
		String celularDuenio = JOptionPane.showInputDialog("Celular del dueño");
		String correoDuenio = JOptionPane.showInputDialog("Correo electronico del dueño");
		this.duenio = new Duenio(nombreDelDuenio, celularDuenio, correoDuenio);
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public Duenio getDuenio() {
		return duenio;
	}

	public void setDuenio(Duenio duenio) {
		this.duenio = duenio;
	}
}
