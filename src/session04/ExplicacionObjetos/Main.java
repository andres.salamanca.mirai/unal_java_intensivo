package session04.ExplicacionObjetos;

public class Main {

	public static void main(String[] args) {
		Mascota benji = new Mascota("Perro", "Benji", "Cocker");
		
		System.out.println(benji.getNombre());
		System.out.println(benji.getTipo());
		System.out.println(benji.getDuenio().getNombre());
	}

}
