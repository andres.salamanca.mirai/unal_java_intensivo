package session01;

public class HolaMundo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Primer hola");
		System.out.println("Hola mundo");
		System.out.println("Tercer hola");

		// Verdadero o falso
		boolean variableBooleana = true;

		// Numericos
		int numeroEntero;
		short numeroMuyPequenio;
		long numeroMasGrande;
		float numeroDecimalFlotante;
		double numeroDecimalDouble;

		// Caracteres
		char unaLetra = 960;
		String cadenasLetras = "\u03c0";

		/**
		 * Operaciones con booleanos
		 */
		boolean variableUno = true;
		boolean variableDos = false;
		boolean resultado;
		// AND
		resultado = variableUno && variableDos;
		System.out.println(resultado);
//		OR
		resultado = variableUno || variableDos;
		System.out.println(resultado);
//		NOT
		resultado = !variableDos;
		System.out.println(resultado);

		/**
		 * Operciones de numeros
		 */
		// sumas
		int numeroA = 5 + 6;
		// resta
		int numeroB = 6 - 8;
		// multiplicacion
		int numeroC = 9 * 8;
		// division
		int numeroD = 5 / 9;
		// modulo
		int numeroE = 5 % 9;
		// Incrementar - Decrementar
		// int numeroF = numeroF++;
		// int numeroG = numeroG--;
		int numeroF = 0;
		numeroF += numeroA;
		double numeroG = 10;
		numeroG -= 0.5;
		numeroG *= 6;
		numeroG /= 5;
		// profe ese - = es proporcional a numeroG = - 0.5?
		
		/**
		 * Operaciones con cadenas de caracteres
		 */
		
		//concatenacion suma
		String cadenaUno = "Hola a todos";
		String cadenaDos = "este es un mensaje";
		String resultadoString = cadenaUno + " " + cadenaDos;
		System.out.println(resultadoString);

		System.out.println(numeroG);

		System.out.println(unaLetra);
		System.out.println(cadenasLetras);
	}

}
