package session08;

import java.util.ArrayList;
import java.util.List;

public class Listas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> enteros = new ArrayList<Integer>();
		
		enteros.add(5);
		enteros.add(0,7);
		enteros.add(1,10);
		enteros.add(99);
		enteros.add(100);
		
		enteros.set(3, 80);
		
		enteros.remove(4);
		
		System.out.println(enteros);
		
		System.out.println("Tamaño de enteros: " +enteros.size());
		
		for (int i=0;i<enteros.size();i++) {
			System.out.println(enteros.get(i));
		}
		System.out.println("Impresion con objetos");
		for (Integer numero : enteros) {
			System.out.println(numero);
		}
	}

}
