package session08;

import java.util.Map;
import java.util.TreeMap;

public class ExplicacionTreeMap {

	public static void main(String[] args) {
		TreeMap<String, String> arbol = new TreeMap<String, String>();
		
		arbol.put("1026", "Andrés Maurcio");
		arbol.put("1020", "Carolina Perez");
		arbol.put("1234", "Carlos Gonzalez");
		arbol.put("9874", "Cristan Copete");
		arbol.put("P036", "Blanca Rojas");
		
		for(Map.Entry<String, String> hoja : arbol.entrySet()) {
			System.out.println(hoja.getKey() + " : " + hoja.getValue());
		}
		
		System.out.println(arbol.get("0036"));
	}

}
