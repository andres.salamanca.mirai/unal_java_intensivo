package session08;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Ejercicio04 {

	public static void main(String[] args) {
		HashMap<Integer, Integer> primerHash = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> segundoHash = new HashMap<Integer, Integer>();

		HashMap<Integer, Integer> repetidos = new HashMap<Integer, Integer>();
		Random r = new Random();

		for (int i = 0; i < 100; i++) {
			int llave = (int) (r.nextDouble() * 60);
			int valor = (int) (r.nextDouble() * 80);

			if (r.nextBoolean())
				primerHash.put(llave, valor);
			if (r.nextBoolean())
				segundoHash.put(llave, valor);
		}

		System.out.println("Tamaño primero: " + primerHash.size() + " Elementos: " + primerHash);
		System.out.println("Tamaño segundo: " + segundoHash.size() + " Elementos: " + segundoHash);

		int cantidadElementosEnDos = 0;
		for (Map.Entry<Integer, Integer> elemento : primerHash.entrySet()) {
			if (segundoHash.containsKey(elemento.getKey()) && segundoHash.containsValue(elemento.getValue())) {
				cantidadElementosEnDos += 1;
				repetidos.put(elemento.getKey(), elemento.getValue());
			}
		}
		System.out.println(cantidadElementosEnDos + " \n" + repetidos);

		cantidadElementosEnDos = 0;
		repetidos.clear();

		for (Map.Entry<Integer, Integer> elemento : primerHash.entrySet()) {
			if (segundoHash.containsKey(elemento.getKey()))
				if (segundoHash.get(elemento.getKey()).equals(elemento.getValue())) {
					cantidadElementosEnDos += 1;
					repetidos.put(elemento.getKey(), elemento.getValue());
				}
		}
		System.out.println(cantidadElementosEnDos + " \n" + repetidos);

	}

}
