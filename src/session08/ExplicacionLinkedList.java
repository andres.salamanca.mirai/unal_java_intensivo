package session08;

import java.util.LinkedList;

public class ExplicacionLinkedList {

	public static void main(String[] args) {
		LinkedList<String> oraciones = new LinkedList<String>();
		
		oraciones.add("Primera oración insertada");
		oraciones.add(1,"Segunda oración insertada");
		oraciones.addFirst("Ahora esta sera la primera oracion");
		oraciones.addLast("Está será la última oración");
		
		System.out.println(oraciones);
		System.out.println(oraciones.getLast());
	}

}
