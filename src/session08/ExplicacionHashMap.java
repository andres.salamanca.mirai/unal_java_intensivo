package session08;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExplicacionHashMap {

	public static void main(String[] args) {
		HashMap<Integer, String> mapaDePersonas = new HashMap<Integer, String>();
		
		mapaDePersonas.put(1026, "Andrés Maurcio");
		mapaDePersonas.put(1020, "Carolina Perez");
		mapaDePersonas.put(1234, "Carlos Gonzalez");
		mapaDePersonas.put(9874, "Cristan Copete");
		
		System.out.println(mapaDePersonas);
		
		if (mapaDePersonas.containsKey(4567)) {
			System.out.println(mapaDePersonas.get(1026));
		}
		
		for(Map.Entry<Integer, String> elemento : mapaDePersonas.entrySet()) {
			System.out.println(elemento.getKey() +" : " + elemento.getValue());
		}
	}

}
