package session08;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ejercicio01 {

	public static void main(String[] args) {
		Random r = new Random();
		
		List<Integer> numeros = new ArrayList<Integer>();
		for( int i=0;i<100;i++) {
			numeros.add((int) (r.nextDouble()*20));
		}
		
		System.out.println(numeros);
		
		List<Integer> numerosProcesados = new ArrayList<Integer>();
		
		for(Integer revision: numeros) {
			if(!numerosProcesados.contains(revision))
				numerosProcesados.add(revision);
		}
		
		System.out.println(numerosProcesados);
		System.out.println(numerosProcesados.size());

	}

}
