package session05;

import java.util.Calendar;
import java.util.Date;

public class Cliente extends Persona{
	
	private Date fechaCreacionCliente;
	
	public Cliente() {
		super();
		this.fechaCreacionCliente = Calendar.getInstance().getTime();
	}

	public Date getFechaCreacionCliente() {
		return fechaCreacionCliente;
	}

	public void setFechaCreacionCliente(Date fechaCreacionCliente) {
		this.fechaCreacionCliente = fechaCreacionCliente;
	}
	
	public double getDineroEnCuenta() {
		return this.dineroEnCuenta;
	}
	
	public void setDineroEnCuenta(double dineroEnCuenta) {
		this.dineroEnCuenta = dineroEnCuenta;
	}
	
}
