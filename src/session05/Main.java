package session05;

import java.util.Date;

import javax.swing.JOptionPane;

import session06.PruebaSatic;

public class Main {

	public static void main(String[] args) {
		Persona p = new Persona();
		Persona andres = new Persona("1026", "Andrés", "Apellidos", "315", "amsalamanca@gmail.com", "Bogotá", 34, 0);

		Cliente clienteUno = new Cliente();

		clienteUno.setNombres(JOptionPane.showInputDialog("Ingrese el nombre del cliente"));
		clienteUno.setApellidos(JOptionPane.showInputDialog("Ingrese el apellido del cliente"));
		
		Date fecha = new Date(121, 0, 15);

		Empleado empleado = new Empleado(fecha, "Gerente oficina Salitre", "Catalina", "Hernández", "Bogotá", "310",
				"1020", 0);
		
		empleado.setDireccion("Medellín");

		String mensaje = "El cliente(a): " + clienteUno.getNombres() + " " + clienteUno.getApellidos() + "\n"
				+ "Creo su cuenta el: " + clienteUno.getFechaCreacionCliente();

		String mensaje2 = "El (la) gerete de la oficina Salitre es: " + empleado.getNombres() + " "
				+ empleado.getApellidos() + "\n" + "Fue contradado (a) el: " + empleado.getFechaDeContrato();
		System.out.println(mensaje);
		System.out.println(mensaje2);
	}

}
