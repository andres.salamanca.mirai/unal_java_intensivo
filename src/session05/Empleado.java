package session05;

import java.util.Date;

public class Empleado extends Persona {
	
	private Date fechaDeContrato;
	private String cargo;
	
	public Empleado(Date fechaContrato, String cargo, String nombre, String apellidos, String direccion, String telefono, String documento, int edad) {
		super(documento,nombre,apellidos,telefono, "por asignar", direccion, edad,0);
		this.fechaDeContrato = fechaContrato;
		this.cargo = cargo;
	}
	
	public void recibirCompensacion(double valorCompensacion) {
		this.dineroEnCuenta += valorCompensacion;
	}

	public Date getFechaDeContrato() {
		return fechaDeContrato;
	}

	public void setFechaDeContrato(Date fechaDeContrato) {
		this.fechaDeContrato = fechaDeContrato;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
	

}
