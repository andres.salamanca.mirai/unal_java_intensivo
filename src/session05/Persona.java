package session05;

public class Persona {

	/**
	 * Atributos
	 */
	private String documento;
	private String nombres;
	private String apellidos;
	private String nombresCompletos;

	private String telefono;
	private String email;
	private String direccion;

	private int edad;
	protected double dineroEnCuenta;

	/**
	 * Constructores
	 */

	public Persona() {
		this.documento = "";
		this.nombres = "";
		this.apellidos = "";
		this.telefono = "";
		this.email = "";
		this.direccion = "";
		this.edad = 18;
		this.dineroEnCuenta = 0;
	}

	public Persona(String documento, String nombres, String apellidos, String telefono, String email, String direccion,
			int edad, double dinero) {
		this.documento = documento;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.telefono = telefono;
		this.email = email;
		this.direccion = direccion;
		this.edad = edad;
		this.dineroEnCuenta = dinero;
	}

	public Persona(String nombres, String apellidos) {
		this.nombres = nombres;
		this.apellidos = apellidos;

		this.nombresCompletos = nombres + " " + apellidos;
	}

	public Persona(String documento, int edad) {
		this.documento = documento;
		this.edad = edad;
	}

	public Persona(int edad, String telefono) {
		this.edad = edad;
		this.telefono = telefono;
	}

	/**
	 * Métodos
	 * 
	 * @param salario
	 */

	public void recibirSalario(double salario) {
		this.dineroEnCuenta += salario;
	}

	public boolean pagarDeuda(double valorDeuda) {
		if (valorDeuda > this.dineroEnCuenta) {
			return false;
		} else {
			this.dineroEnCuenta -= valorDeuda;
			return true;
		}
	}
	
	/**
	 * Get y Set
	 */


	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombresCompletos() {
		return nombresCompletos;
	}

	public void setNombresCompletos(String nombresCompletos) {
		this.nombresCompletos = nombresCompletos;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	
		
}
