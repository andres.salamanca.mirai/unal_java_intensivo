package session13;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		ExplicacionInterfazGrafica pantalla = new ExplicacionInterfazGrafica();
		
		pantalla.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pantalla.setSize(200,200);
		pantalla.setVisible(true);
	}

}
