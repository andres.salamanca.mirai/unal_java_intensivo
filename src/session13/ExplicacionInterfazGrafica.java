package session13;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class ExplicacionInterfazGrafica extends JFrame{
	
	private JLabel etiqueta;
	private JLabel etiquetaDos;
	private JTextField campoTexto;
	private JTextField campoTextoDos;
	
	private JPasswordField contrasenia;
	
	private JButton boton;
	
	public ExplicacionInterfazGrafica() {
		//Nombre de la ventana
		super("Nueva login");
		//Orden de los elementos
		setLayout(new FlowLayout());
		//Manejo de nuestra etiqueta
		etiqueta = new JLabel("Usuario: ");
		etiqueta.setToolTipText("Esta es mi primera etiqueta en java");
		add(etiqueta);
		//Manejo de un campo de texto
		campoTexto = new JTextField(10);
		add(campoTexto);
		
		//Etiqueta dos
		etiquetaDos = new JLabel("Contraseña: ");
		add(etiquetaDos);
		
		contrasenia = new JPasswordField(10);
		add(contrasenia);
		
		//Campo de texto dos
		campoTextoDos = new JTextField(10);
//		add(campoTextoDos);
		
		boton = new JButton("Nuestro botón");
		add(boton);
		
		Controlador controlador = new Controlador();
		boton.addActionListener(controlador);
		
	}
	
	private class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent evento) {
			String cadena = "";
			if(evento.getSource() == boton) {
				cadena = campoTexto.getText();
				cadena += "\n" + contrasenia.getText();
			}
			JOptionPane.showMessageDialog(null, cadena);
		}

	}

}
