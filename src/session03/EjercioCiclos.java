package session03;

public class EjercioCiclos {

	/**
	 * En 2022 el pais A tendra una poblacion de 25 millones de habitantes y el pais
	 * B de 18.9 millones. Las tasas de crecimiento anual de la poblacion seran de
	 * 2% y 3% respectivamente. Desarrollar un algoritmo para informar en que año la
	 * poblacion del pais B superara a la de A.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/**
		 * Version For
		 */

		double poblacionA = 25;
		double tasaA = 0.02;
		double poblacionB = 18.9;
		double tasaB = 0.03;
		int anio = 2022;

		for (anio = 2023; poblacionA > poblacionB; anio++) {
			poblacionA = poblacionA + (poblacionA * tasaA);
			poblacionB += poblacionB * tasaB;
			System.out.println("año: "+anio+"\tA:"+poblacionA+"\tB:"+poblacionB);
		}
		
		/**
		 * Version While
		 */
		
		poblacionA = 25;
		tasaA = 0.02;
		poblacionB = 18.9;
		tasaB = 0.03;
		anio = 2022;
		
		while(poblacionA > poblacionB) {
			anio += 1;
			poblacionA = poblacionA + (poblacionA * tasaA);
			poblacionB += poblacionB * tasaB;
			System.out.println("año: "+anio+"\tA:"+poblacionA+"\tB:"+poblacionB);
		}
		
		/** 
		 * Version do while
		 */
		
		poblacionA = 25;
		tasaA = 0.02;
		poblacionB = 18.9;
		tasaB = 0.03;
		anio = 2022;
		
		do {
			anio += 1;
			poblacionA = poblacionA + (poblacionA * tasaA);
			poblacionB += poblacionB * tasaB;
			System.out.println("año: "+anio+"\tA:"+poblacionA+"\tB:"+poblacionB);
		}while(poblacionA>poblacionB);

	}

}
