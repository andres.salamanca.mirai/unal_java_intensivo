package session03;

import javax.swing.JOptionPane;

public class ExplicacionDoWhile {

	public static void main(String[] args) {
		int i = 0;
		do{
			/**
			 * En python lo poniamos asi:
			 * 
			 * i = (int) input("Ingresa el valor de i)
			 */
			i = Integer.parseInt(JOptionPane.showInputDialog("Ingresa el valor de i"));
			System.out.println("El valor de i es: "+ i);
		}while(i!=0);
		
	}

}
