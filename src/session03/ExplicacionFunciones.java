package session03;

import javax.swing.JOptionPane;

public class ExplicacionFunciones {

	public static void main(String[] args) {
		//double resultado = suma(5.6, 4.5);
		//System.out.println(resultado);

		saludar("Albeiro");
		
		boolean esMayorDeEdad = mayorDeEdad();
		if(esMayorDeEdad)
			System.out.println("Si es mayor de edad");
		else
			System.out.println("No tienes que pagar impuestos");
	}

	public static boolean mayorDeEdad() {
		int edad = Integer.parseInt(JOptionPane.showInputDialog("Cual es tu edad"));
		
		if (edad <18) 
			return false;
		else 
			return true;
		
	}

	public static void saludar(String nombre) {
		JOptionPane.showMessageDialog(null, "Hola!!!\n" + nombre);
		JOptionPane.showMessageDialog(null, "El resultado de la suma es: " + suma(1.2, 3.4));
	}

	public static double suma(double numeroUno, double numeroDos) {
		return numeroUno + numeroDos;

	}

}
