package session03;

public class ExplicacionFor {

	public static void main(String[] args) {

		for (int i = 1; i <= 10; i++) {
			int dado = (int) (1 + Math.random() * 6);
			System.out.println("Vamos a lanzar el dado por " + i + " vez! " + dado);
		}

		/**
		 * Incrementar de dos en dos
		 */
		for (int i = 1; i <= 10; i += 2) {
			int dado = (int) (1 + Math.random() * 6);
			System.out.println("Vamos a lanzar el dado por " + i + " vez! " + dado);
		}

		for (int i = 10; i > 0; i--) {
			int dado = (int) (1 + Math.random() * 6);
			System.out.println("Vamos a lanzar el dado por " + i + " vez! " + dado);
		}

		/**
		 * Decrementar de tres en tres
		 */
		for (int i = 10; i > 0; i -= 3) {
			int dado = (int) (1 + Math.random() * 6);
			System.out.println("Vamos a lanzar el dado por " + i + " vez! " + dado);
		}
		int numero = 0;
		for (int i = 0; i < 100; i += numero) {
			numero = (int) (1 + Math.random() * 6);
			System.out.println("Vamos a lanzar el dado por " + i + " vez! " + numero);
			if (numero == 5) {
				break;
			}
		}
		for (int i = 0; i < 10; i++)
			System.out.println(i);
	}

}
