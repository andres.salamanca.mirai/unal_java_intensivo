package session14;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class ComunicacionBaseDatos {

	public static void main(String[] args) {
		String url = "jdbc:mysql://unal-ciclo2-intensivo.cfynfmhdadhz.us-east-2.rds.amazonaws.com:3306/LIBRERIA_ANDRES_SALAMANCA";
		String usuario = "unal";
		String contrasenia = "UNAL2021";

		try {
			Connection conexion = DriverManager.getConnection(url, usuario, contrasenia);
			if (conexion != null) {
				System.out.println("Conectado");
			}

			
			/**
			 * Insertar datos en la base de datos
			 */
			
			String sql = " INSERT INTO AUTOR (APELLIDOS, NOMBRES, EMAIL, PAIS) VALUES (?,?,?,?)";
			
			PreparedStatement sentenciaInsersion = conexion.prepareStatement(sql);
			sentenciaInsersion.setString(1, JOptionPane.showInputDialog("Apellidos"));
			sentenciaInsersion.setString(2, JOptionPane.showInputDialog("Nombres"));
			sentenciaInsersion.setString(3, JOptionPane.showInputDialog("Correo"));
			sentenciaInsersion.setString(4, JOptionPane.showInputDialog("Nacionalidad"));
			
			int resultadoInsersion = sentenciaInsersion.executeUpdate();
			
			System.out.println(resultadoInsersion);
			if(resultadoInsersion>0)
				System.out.println("Informacion ingresada");
			
			/**
			 * Obtener datos de la base de datos
			 */
			sql = "SELECT * FROM AUTOR";
			Statement sentencia = conexion.createStatement();
			ResultSet resultadoConsulta = sentencia.executeQuery(sql);

			int contador = 0;
			while (resultadoConsulta.next()) {
				int id = resultadoConsulta.getInt(1);
				String apellidos = resultadoConsulta.getString(2);
				String nombres = resultadoConsulta.getString(3);
				String email = resultadoConsulta.getString(4);
				String pais = resultadoConsulta.getString(5);

				System.out.println(id + "\t" + apellidos + "\t" + nombres + "\t" + email + "\t" + pais);
			}
			
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
