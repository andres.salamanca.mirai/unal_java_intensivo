package session14;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ExplicacionCheckBox extends JFrame {
	private JCheckBox negrilla;
	private JCheckBox cursiva;
	private JTextField campoTexto;
	private JPanel panel;

	public ExplicacionCheckBox() {
		super("Explicacion CheckBox");
		setLayout(new BorderLayout());

		campoTexto = new JTextField("Observe el cambio del formato");
		campoTexto.setFont(new Font("Serif", Font.PLAIN, 14));
		add(campoTexto,BorderLayout.CENTER);

		negrilla = new JCheckBox("Negrilla");
		cursiva = new JCheckBox("Cursiva");

		panel = new JPanel();
		panel.add(negrilla);
		panel.add(cursiva);
		
		add(panel,BorderLayout.NORTH);
		
		add(negrilla,BorderLayout.EAST);

		Manejador manager = new Manejador();
		negrilla.addItemListener(manager);
		cursiva.addItemListener(manager);

	}

	private class Manejador implements ItemListener {
		private int valNegrita = Font.PLAIN;
		private int valCursiva = Font.PLAIN;

		@Override
		public void itemStateChanged(ItemEvent evento) {
			if (evento.getSource() == negrilla)
				if (negrilla.isSelected())
					valNegrita = Font.BOLD;
				else
					valNegrita = Font.PLAIN;
			if (evento.getSource() == cursiva)
				if (cursiva.isSelected())
					valCursiva = Font.ITALIC;
				else
					valCursiva = Font.PLAIN;

			campoTexto.setFont(new Font(null, valNegrita + valCursiva, 14));

		}

	}
}
