package session06;

public class PruebaFinal {
	
	public final double SALUD_PORCENTAJE = 0.12;
	public final double PENSION_PORCENTAJE = 0.16;
	
	public final double getValorSalud(double salario) {
		return salario * this.SALUD_PORCENTAJE;
	}
	
	public final double getValorPension(double salario) {
		return salario * this.PENSION_PORCENTAJE;
	}
	
}
