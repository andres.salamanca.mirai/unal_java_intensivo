package session06;

public interface PersonaJuridica {
	
	public double enviarFactura(double valorVenta);
	public double pagarImpuestos(double valorIngresos, int aniosCreacion);
	public boolean renovarCamaraYComercio();

}
