package session06.triqui;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		Tablero tablero = new Tablero();
		
		while (tablero.verificarGanador().equals("a")) {
			int fila = Integer.parseInt(JOptionPane.showInputDialog("O: Ingrese fila"));
			int columna = Integer.parseInt(JOptionPane.showInputDialog("O: Ingrese columna"));
			tablero.marcarCasilla("O", fila, columna);
			fila = Integer.parseInt(JOptionPane.showInputDialog("X: Ingrese fila"));
			columna = Integer.parseInt(JOptionPane.showInputDialog("X: Ingrese columna"));
			tablero.marcarCasilla("X", fila, columna);

			JOptionPane.showMessageDialog(null, tablero.verTablero());
		}
		JOptionPane.showMessageDialog(null, "El ganador es:" + tablero.verificarGanador());
	}

}
