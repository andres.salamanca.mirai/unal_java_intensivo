package session06.triqui;

import session05.Persona;

public class Tablero {

	private String tablero[][] = new String[3][3];

	public Tablero() {
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				tablero[i][j] = "a";
			}
		}
	}

	public void marcarCasilla(String simbolo, int fila, int columna) {
		tablero[fila][columna] = simbolo;
	}

	public String verificarGanador() {
		// Rojo
		if (tablero[0][0].equals(tablero[1][1]) && tablero[0][0].equals(tablero[2][2]))
			return tablero[0][0];
		if (tablero[0][0].equals(tablero[0][1]) && tablero[0][0].equals(tablero[0][2]))
			return tablero[0][0];
		if (tablero[0][0].equals(tablero[1][0]) && tablero[0][0].equals(tablero[2][0]))
			return tablero[0][0];

		// Amarillo
		if (tablero[0][1].equals(tablero[1][1]) && tablero[0][1].equals(tablero[2][1]))
			return tablero[0][1];

		// Azul
		if (tablero[0][2].equals(tablero[1][1]) && tablero[0][2].equals(tablero[2][0]))
			return tablero[0][2];
		if (tablero[0][2].equals(tablero[1][2]) && tablero[0][2].equals(tablero[2][2]))
			return tablero[0][2];

		// Negro
		if (tablero[1][0].equals(tablero[1][1]) && tablero[1][0].equals(tablero[1][2]))
			return tablero[1][0];
		if (tablero[2][0].equals(tablero[2][1]) && tablero[2][0].equals(tablero[2][2]))
			return tablero[2][0];
		return "a";

	}

	public String verificarCasilla(int fila, int casilla) {
		return tablero[fila][casilla];
	}
	
	public String verTablero() {
		String resultado= "";
		
		for (int i = 0; i < this.tablero.length; i++) {
			for (int j = 0; j < this.tablero.length; j++) {
				resultado += this.tablero[i][j] +" ";
			}
			resultado +="\n";
		}
		return resultado;
	}
	
}
