package session07.ejericio04;

public interface Operaciones {
	
	public double suma (double a, double b);
	public double resta (double a, double b);
	public double multiplicacion (double a, double b);
	public static double division (double a, double b) {
		return a/b;
	}

}
