package session07.ejericio01;

public class Persona {
	
	private String nombres;
	private String apellidos;
	private int edad;
	
	// Constructor
	public Persona (String nombres, String apellidos, int edad) {
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.edad = edad;
	}

	public String getNombres() {
		return nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public int getEdad() {
		return edad;
	}
	
	public String toString() {
		return "Nombre: "+getNombres()+ " " + getApellidos() + ", edad: "+ getEdad();
	}
}
