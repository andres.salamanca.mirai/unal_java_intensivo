package session07.ejericio01;

import java.util.Date;

public class Profesor extends Empleado {
	
	private String asignatura;
	private String lineasInvestigacion;

	public Profesor(String nombres, String apellidos, int edad, double salario, String cargo, Date fechaContrato) {
		super(nombres, apellidos, edad, salario, cargo, fechaContrato);
		// TODO Auto-generated constructor stub
	}

	public String getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}

	public String getLineasInvestigacion() {
		return lineasInvestigacion;
	}

	public void setLineasInvestigacion(String lineasInvestigacion) {
		this.lineasInvestigacion = lineasInvestigacion;
	}
	
	

}
