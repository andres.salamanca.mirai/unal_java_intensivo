package session07.ejericio01;

import java.util.Date;

public class Empleado extends Persona {

	private double salario;
	private String cargo;
	private Date fechaContrato;

	public Empleado(String nombres, String apellidos, int edad, double salario, String cargo, Date fechaContrato) {
		super(nombres, apellidos, edad);
		this.salario = salario;
		this.cargo = cargo;
		this.fechaContrato = fechaContrato;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Date getFechaContrato() {
		return fechaContrato;
	}

	public void setFechaContrato(Date fechaContrato) {
		this.fechaContrato = fechaContrato;
	}

	public String toString() {
		return super.toString() + ", salario: " + this.salario + ", fecha de contrato: " + fechaContrato;
	}

}
