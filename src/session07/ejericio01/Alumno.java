package session07.ejericio01;

public class Alumno extends Persona {

	public String programa;
	public int semestre;

	public Alumno(String nombres, String apellidos, int edad) {
		super(nombres, apellidos, edad);
	}

	public String getPrograma() {
		return programa;
	}

	public void setPrograma(String programa) {
		this.programa = programa;
	}

	public int getSemestre() {
		return semestre;
	}

	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}
}
