package session07.ejericio01;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.JOptionPane;

public class Main {

	private static List<Persona> personalInstitucion = new ArrayList<Persona>();

	public static void main(String arg[]) {

		String menu = "Seleccione una opción:\n" + "1. Agregar un directivo\n" + "2. Agregar un profesor\n"
				+ "3. Agregar un estudiante\n" + "4. Agregar un administrativo\n" + "5. Ver todo el personal\n"
				+ "6. Salir";
		int opcion = 0;
		do {
			opcion = Integer.parseInt(JOptionPane.showInputDialog(menu));
			ejecutarOpcion(opcion);
		} while (opcion != 6);
	}

	private static void ejecutarOpcion(int opcion) {
		switch (opcion) {
		case 1:
			agregarDirectivo();
			break;
		case 2:
			agregarProfesor();
			break;
		case 3:
			agregarEstudiante();
			break;
		case 4:
			agregarAdministrativo();
			break;
		case 5:
			verTodoElPersonal();
			break;
		default:
			break;
		}
	}

	private static void verTodoElPersonal() {
		for (int i = 0; i < personalInstitucion.size(); i++)
			System.out.println(personalInstitucion.get(i));
	}

	private static void agregarAdministrativo() {

		String nombres = JOptionPane.showInputDialog(null, "Nombres", "Administrativo", 0);
		String apellidos = JOptionPane.showInputDialog(null, "Apellidos", "Administrativo", 0);
		int edad = Integer.parseInt(JOptionPane.showInputDialog(null, "Edad", "Administrativo", 0));
		double salario = Double.parseDouble(JOptionPane.showInputDialog(null, "Salario", "Administrativo", 0));
		String cargo = JOptionPane.showInputDialog(null, "Cargo", "Directivo", 0);

		Administrativo a = new Administrativo(nombres, apellidos, edad, salario, cargo, null);
		personalInstitucion.add(a);
	}

	private static void agregarEstudiante() {
		String nombres = JOptionPane.showInputDialog(null, "Nombres", "Estudiante", 0);
		String apellidos = JOptionPane.showInputDialog(null, "Apellidos", "Estudiante", 0);
		int edad = Integer.parseInt(JOptionPane.showInputDialog(null, "Edad", "Estudiante", 0));

		Alumno a = new Alumno(nombres, apellidos, edad);
		personalInstitucion.add(a);
	}

	private static void agregarProfesor() {
		String nombres = JOptionPane.showInputDialog(null, "Nombres", "Profesor", 0);
		String apellidos = JOptionPane.showInputDialog(null, "Apellidos", "Profesor", 0);
		int edad = Integer.parseInt(JOptionPane.showInputDialog(null, "Edad", "Profesor", 0));
		double salario = Double.parseDouble(JOptionPane.showInputDialog(null, "Salario", "Profesor", 0));
		String cargo = JOptionPane.showInputDialog(null, "Cargo", "Directivo", 0);

		Profesor p = new Profesor(nombres, apellidos, edad, salario, cargo, null);
		personalInstitucion.add(p);
	}

	private static void agregarDirectivo() {
		String nombres = JOptionPane.showInputDialog(null, "Nombres", "Directivo", 0);
		String apellidos = JOptionPane.showInputDialog(null, "Apellidos", "Directivo", 0);
		int edad = Integer.parseInt(JOptionPane.showInputDialog(null, "Edad", "Directivo", 0));
		double salario = Double.parseDouble(JOptionPane.showInputDialog(null, "Salario", "Directivo", 0));
		String cargo = JOptionPane.showInputDialog(null, "Cargo", "Directivo", 0);

		Directivo directivo = new Directivo(nombres, apellidos, edad, salario, cargo, Calendar.getInstance().getTime());
		personalInstitucion.add(directivo);
	}

}
