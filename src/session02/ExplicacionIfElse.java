package session02;

import javax.swing.JOptionPane;

public class ExplicacionIfElse {

	public static void main(String[] args) {
		
		boolean juicioValor = true;
		boolean juicioDos = false;
		
		System.out.println(juicioDos);
		
		if (juicioValor && juicioDos) {
			System.out.println("Juicio valor es verdadero");
		}else {
			System.out.println("Juicio de valor es falso");
		}
		
		/**
		 * Para comparar tenemos las siguientes opciones:
		 * 
		 * Exactamente igual ==
		 * Distintos !=
		 * Mayor >
		 * Menor <
		 * Mayor igual >=
		 * Menor igual <=
		 * 
		 */
		
		String nombrePersona = JOptionPane.showInputDialog("Como es tu nombre");
		
		if(nombrePersona.equals("Edwin")) {
			JOptionPane.showMessageDialog(null, "Hola Edwin");
		}else {
			JOptionPane.showMessageDialog(null, "Tu no eres Edwin");
		}
		
		int numero = Integer.parseInt(JOptionPane.showInputDialog("Ingresa un número"));
		double dinero = Double.parseDouble(JOptionPane.showInputDialog("Cuanto dinero pediste prestado"));
		if (numero > 10) {
			JOptionPane.showMessageDialog(null, "Numero mayor a 10", "El numero es mayor a 10", JOptionPane.WARNING_MESSAGE);
		}

	}

}
