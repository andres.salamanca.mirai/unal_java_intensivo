package session02;

import java.text.DecimalFormat;

import javax.swing.JOptionPane;

public class ExplicacionSwitch {

	public static void main(String[] args) {

		String mensaje = "Hola a todos\n" + "Tenmos los numeros 5 y 7\n" + "Para sumar presione 1\n"
				+ "Para restar presione 2\n" + "Para dividir presione 3";

		int opcion = Integer.parseInt(JOptionPane.showInputDialog(mensaje));

		switch (opcion) {
		case 1: {
			JOptionPane.showMessageDialog(null, "El resultado es: \t" + (5 + 7));
			break;
		}
		case 2: {
			JOptionPane.showMessageDialog(null, "El resultado es: \t" + (5 - 7));
			break;
		}
		case 3: {
			DecimalFormat df = new DecimalFormat("0.00");
			JOptionPane.showMessageDialog(null, "El resultado es: \t" + df.format(500.0 / 7.0) );
			break;
		}
		default: {
			JOptionPane.showMessageDialog(null, "Opcion erronea");
			break;
		}
		}

	}

}
